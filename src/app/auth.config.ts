import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  strictDiscoveryDocumentValidation: false,
  skipIssuerCheck: true,
  redirectUri: 'http://localhost:4200',
  silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
  clientId: '1928085f-8057-4219-a1dc-ac93fd287c3a',
  scope: 'openid profile email user_impersonation',
  resource: 'https://fox-contact.markarian-ota.nl/',
  showDebugInformation: true,
  sessionChecksEnabled: true
}
